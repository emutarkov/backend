from datetime import datetime, timedelta, timezone

import freezegun
import httpx
import pytest
from starlette import status

endpoint_url = "/client/weather"


@pytest.fixture
def now() -> datetime:
    now = datetime.now(tz=timezone.utc)
    with freezegun.freeze_time(now):
        return now


@pytest.fixture
async def response(
    http_client: httpx.AsyncClient,
    now: datetime,  # noqa: ARG001
) -> httpx.Response:
    return await http_client.post(endpoint_url)


async def test_returns_200(response: httpx.Response) -> None:
    assert response.status_code == status.HTTP_200_OK


async def test_should_return_correct_time_in_weather(
    response: httpx.Response, now: datetime
) -> None:
    delta = timedelta(
        hours=now.hour,
        minutes=now.minute,
        seconds=now.second,
        microseconds=now.microsecond,
    )
    weather = response.json()["data"]
    accelerated_time = now + delta * weather["acceleration"]
    accelerated_time.replace(
        year=now.year,
        month=now.month,
        day=now.day,
    )
    date_str = accelerated_time.strftime("%Y-%m-%d")
    time_str = accelerated_time.strftime("%H:%M:%S")

    assert weather["weather"]["timestamp"] == int(now.timestamp())

    assert weather["weather"]["date"] == date_str
    assert weather["date"] == date_str

    assert weather["weather"]["time"] == accelerated_time.strftime("%Y-%m-%d %H:%M:%S")
    assert weather["time"] == time_str
