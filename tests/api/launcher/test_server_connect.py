from pathlib import Path

import httpx
import pytest
from fastapi import status


@pytest.fixture
async def response(http_client: httpx.AsyncClient) -> httpx.Response:
    return await http_client.get("/launcher/server/connect")


def test_returns_200(response: httpx.Response) -> None:
    assert response.status_code == status.HTTP_200_OK


def test_should_contain_server_name(
    server_name: str,
    response: httpx.Response,
) -> None:
    assert response.json()["name"] == server_name


def test_backend_url(response: httpx.Response, base_url: str) -> None:
    assert response.json()["backendUrl"] == base_url


def test_edition_list(response: httpx.Response) -> None:
    starting_profiles_dir = Path("resources/database/starting_profiles").glob("*")
    available_editions = sorted(d.name for d in starting_profiles_dir if d.is_dir())
    assert response.json()["editions"] == available_editions
