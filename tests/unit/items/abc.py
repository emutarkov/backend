from typing import Protocol

from modules.items.types import Item


class MakeItem(Protocol):
    def __call__(
        self,
        template_id: str | None = None,
        name: str | None = None,
        stack_count: int = 1,
    ) -> Item:
        ...
