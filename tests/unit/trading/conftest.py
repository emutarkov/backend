import pytest

import paths


@pytest.fixture
def trader_ids() -> list[str]:
    return [path.stem for path in paths.traders.iterdir()]
