from pathlib import Path

from _pytest._py.path import LocalPath

from utils import generate_certificates


def test_creates_two_certificate_files(tmpdir: LocalPath) -> None:
    generate_certificates(Path(tmpdir))
    assert tmpdir.join("certificate.pem").exists()  # type: ignore[no-untyped-call, arg-type]
    assert tmpdir.join("key.pem").exists()  # type: ignore[no-untyped-call, arg-type]
