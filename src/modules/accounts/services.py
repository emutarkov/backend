from typing import Annotated

from aioinject import Inject
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from database.models import Account

from . import schema


class AccountService:
    def __init__(self, session: Annotated[AsyncSession, Inject]) -> None:
        self.session = session

    async def create_account(
        self,
        model: schema.AccountCreate,
    ) -> Account:
        account = Account(
            username=model.username,
            password=model.password,
            edition=model.edition,
        )
        self.session.add(account)
        await self.session.commit()
        await self.session.refresh(account)
        return account

    async def login(self, model: schema.AccountLogin) -> Account | None:
        stmt = select(Account).filter(
            Account.username == model.username,
            Account.password == model.password,
        )
        account: Account | None = await self.session.scalar(stmt)
        return account

    async def is_username_taken(self, username: str) -> bool:
        stmt = select(select(Account).filter(Account.username == username).exists())
        exists: bool = await self.session.scalar(stmt)
        return exists
